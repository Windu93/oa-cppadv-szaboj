#include <string>
#include <random>

using std::string;

class Util
{
private:
    static std::random_device rd;
    static std::default_random_engine generator;
    
public:
    Util(/* args */);
    ~Util();

    template<typename T>
    static T random(T min, T max)
    {
        std::uniform_int_distribution<int> distribution(min, max);
        return distribution(generator);
    }

    template<typename T>
    static bool isPrime(T number)
    {
		for (int i = 2; i<= sqrt(number); ++i) {
			if (number % i == 0) {
				return false;
			}
		}

        return true;
    }

    template<typename T>
    static std::string formatList(const T & container)
    {
        string ret_string("");
        for (const auto & i : container)
            ret_string += std::to_string(i) + " ";
        return ret_string;
    }
};

std::random_device Util::rd;
std::default_random_engine Util::generator(rd());