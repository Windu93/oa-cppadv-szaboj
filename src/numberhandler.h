#pragma once

#include <functional>

template<typename T, typename CONTAINER>
class NumberHandler
{
private:
    CONTAINER container;
public:
    NumberHandler(/* args */) {}
    ~NumberHandler() {}
    
    void fill(int count, std::function< T()> generator)
    {
        for (int i = 0; i < count; i++)
            container.push_back(generator());
    }

    CONTAINER select (std::function< bool(T)> filter)
    {
        CONTAINER ret_cont;
        for (const auto & it : container)
        {
            if (filter(it))
                ret_cont.push_back(it);
        }

        return ret_cont;
    }

    const CONTAINER list () const
    {
        return container;
    }
};

